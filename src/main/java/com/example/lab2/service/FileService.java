package com.example.lab2.service;

import com.example.lab2.Model.File;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.stream.Stream;

public interface FileService {

     File store(MultipartFile file, Integer id) throws IOException;

     File getFile(String id);
    Stream<File> getAllFiles();
    void deleteFile(String id);

}
