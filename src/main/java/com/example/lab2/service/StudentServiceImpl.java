package com.example.lab2.service;

import com.example.lab2.Model.Student;
import com.example.lab2.Repository.StudentRepository;
import com.example.lab2.controller.ApiResponse;
import com.example.lab2.controller.ProjektController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;



@Service
public class StudentServiceImpl implements StudentService{
    private static final Logger logger = LoggerFactory.getLogger(ProjektController.class);

    @Autowired
    StudentRepository studentRepo;
    @Autowired
    private JavaMailSender sender;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public List<Student> getAllUsers() {
        return studentRepo.findAll();
    }

    @Override
    public Student findByEmail(String email) throws Exception {
        return studentRepo.findByEmail(email).orElseThrow(()->new Exception("User Not found.."));
    }

    @Override
    public Student getUserDetailById(int userId) throws Exception {

        return studentRepo.findById(userId).orElseThrow(()->new Exception("User Not found.."));
    }

    @Override
    public Student registerUser(Student student) throws Exception {

        if (studentRepo.findByEmail(student.getEmail()).isPresent()) {
            throw new Exception("Użytkownik z podanym adresem email już istnieje");
        }else {
            student.setRole("USER");
            student.setPassword(passwordEncoder.encode(student.getPassword()));
            sendEmail(student);
            return studentRepo.save(student);
        }
    }
    public ResponseEntity<? extends Object> sendEmail(Student student) {


        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(student.getEmail());
        msg.setSubject("Użytkownik zarejestrował się!");
        msg.setText("Zarejestrowałeś się na platformę projektową");
        sender.send(msg);
        return new ResponseEntity<String>(HttpStatus.OK);
    }


}
