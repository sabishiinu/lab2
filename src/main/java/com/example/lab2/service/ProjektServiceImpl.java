package com.example.lab2.service;

import com.example.lab2.JWTConfiguration.UserPrincipal;
import com.example.lab2.Model.Projekt;
import com.example.lab2.Model.Student;
import com.example.lab2.Repository.ProjektRepository;
import com.example.lab2.Repository.StudentRepository;
import com.example.lab2.Repository.ZadanieRepository;
import com.example.lab2.controller.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;

@Service
public class ProjektServiceImpl implements ProjektService {

    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private ProjektRepository projektRepository;
    @Autowired
    private ZadanieRepository zadanieRepository;
    @Autowired
    private ProjektRepository projektRepo;
    @Autowired
    private StudentService studentService;
    private static final Logger logger = LoggerFactory.getLogger(ProjektServiceImpl.class);

    @Override
    public Optional<Projekt> getProjekt(Integer projektId) {
        return projektRepository.findById(projektId);
    }

    @Override
    public Projekt getProjektById(Integer projektId) {
        return projektRepository.findProjektByProjektId(projektId);
    }

    @Override
    public Projekt setProjekt(Projekt projekt) {
//TODO
        return null;
    }

    @Override
    public void deleteProjekt(Integer projektId) {
        zadanieRepository.deleteAll(zadanieRepository.findZadaniaProjektu(projektId));
        projektRepository.deleteById(projektId);
    }

    @Override
    public Page<Projekt> getProjekty(Pageable pageable) {
//TODO
        return null;
    }

    @Override
    public Page<Projekt> searchByNazwa(String nazwa, Pageable pageable) {
//TODO
        return null;
    }

    @Override
    public List<Projekt> getAllProjekty() {

           return projektRepository.findAll();
    }

    @Override
    public Projekt addProjekt(HashMap<String, String> addRequest) throws Exception {
        try {

            Projekt objects = new Projekt();

            objects.setNazwa(addRequest.get("nazwa"));
            objects.setOpis(addRequest.get("opis"));
            objects.setDataCzasUtworzenia(LocalDateTime.now());
            objects.setDataCzasModyfikacji(LocalDateTime.now());
            objects.setDataOddania(LocalDate.parse(addRequest.get("dataOddania")));

            UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            Student student = studentService.getUserDetailById(userPrincipal.getId());
            objects.addStudent(student);
            projektRepo.save(objects);
            return objects;
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public Projekt updateProjekt(HashMap<String, String> projekt,
                                 @PathVariable Integer projektId) throws Exception {

        Projekt objects = getProjektById(projektId);
        if (projekt.get("nazwa") != "") {
            objects.setNazwa(projekt.get("nazwa"));
        }
        if (projekt.get("opis") != "") {
            objects.setOpis(projekt.get("opis"));
        }
        if(projekt.get("dataOddania")!=""){
            objects.setDataOddania(LocalDate.parse(projekt.get("dataOddania")));
        }

        objects.setDataCzasModyfikacji(LocalDateTime.now());
        return projektRepo.save(objects);
    }

    @Override
    public void joinStudent(Integer id) throws Exception {
        Projekt objects = projektRepo.getById(id);

        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Student student = studentService.getUserDetailById(userPrincipal.getId());
        objects.addStudent(student);
        projektRepo.save(objects);
    }
    @Override
    public Set<Student> getStudents(Integer id) throws Exception {
        Projekt objects = projektRepo.getById(id);
        Set<Student> students = objects.getStudent();
        logger.info("studenci "+students);
        return students;
    }
    @Override
    public List<Projekt> getStudentProjects() throws Exception {

        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Student student = studentService.getUserDetailById(userPrincipal.getId());
        List<Projekt> projects = projektRepo.findProjektsByStudentId();//student.getStudentId())
        logger.info("studenci "+student);
        logger.info("projects "+projects);
        return projects;
    }
    @Override
    public Set<Projekt> get1() throws Exception {
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Student students = studentService.getUserDetailById(userPrincipal.getId());
        return students.getProjekty();
    }



}


