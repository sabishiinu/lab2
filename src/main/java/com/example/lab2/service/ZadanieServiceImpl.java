package com.example.lab2.service;


import com.example.lab2.Model.Projekt;
import com.example.lab2.Model.Zadanie;
import com.example.lab2.Repository.ProjektRepository;
import com.example.lab2.Repository.ZadanieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static java.lang.Integer.parseInt;

@Service
public class ZadanieServiceImpl implements ZadanieService{


    @Autowired
    private ZadanieRepository zadanieRepo;
    @Autowired
    private ProjektService projektService;
    @Autowired
    private StudentService studentService;
    private static final Logger logger = LoggerFactory.getLogger(ProjektServiceImpl.class);

    @Override
    public Zadanie getZadanie(Integer id) {
        return zadanieRepo.findZadanieByZadanieId(id);
    }
    @Override
    public List<Zadanie> getZadanieById(Integer id) {
        return zadanieRepo.findZadaniaProjektu(id);
    }
    @Override
    public Page<Zadanie> getZadanieById(Integer id, Pageable pageable) {
        return zadanieRepo.findZadaniaProjektu(id,pageable);
    }
    @Override
    public Zadanie getZadanieByIdI(Integer id) {
        return (Zadanie) zadanieRepo.findZadaniaProjektu(id);
    }

    @Override
    public Zadanie setZadanie(Zadanie zadanie) {
//TODO
        return null;
    }
    @Override
    public void deleteZadanie(Integer id) {
        zadanieRepo.deleteById(id);
    }

    @Override
    public Page<Zadanie> getZadania(Pageable pageable) {
        return zadanieRepo.findAll(pageable);

    }
    @Override
    public     Page<Zadanie> getZadania1(int page, int size, String sortBy){

        Pageable paging = PageRequest.of(page, size, Sort.by(sortBy));
        return zadanieRepo.findAll(paging);
    }
    @Override
    public Page<Zadanie> searchByNazwa(String nazwa, Pageable pageable) {
//TODO
        return null;
    }
    @Override
    public List<Zadanie> getAllZadania() {
        return zadanieRepo.findAll();
    }

    @Override
    public Zadanie addZadanie(HashMap<String, String> addRequest) throws Exception{
        try {

            Zadanie objects = new Zadanie();
            Projekt projekt = new Projekt();
            projekt = projektService.getProjektById(parseInt(addRequest.get("projektId")));
            objects.setProjekt(projekt);
            objects.setNazwa(addRequest.get("nazwa"));
            objects.setOpis(addRequest.get("opis"));
            objects.setKolejnosc(addRequest.get("kolejnosc"));
            objects.setDataCzasUtworzenia(LocalDateTime.now());
            objects.setDataCzasModyfikacji(LocalDateTime.now());
            objects.setDataOddania(LocalDate.parse(addRequest.get("dataOddania")));
            zadanieRepo.save(objects);
            return objects;
        }catch(Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    @Override
    public Zadanie updateZadanie(HashMap<String,String> zadanie,
                                 @PathVariable Integer id) throws Exception {

        Zadanie objects = getZadanie(id);
        if(zadanie.get("nazwa") != ""){
            objects.setNazwa(zadanie.get("nazwa"));
        }
        if(zadanie.get("opis") != ""){
            objects.setOpis(zadanie.get("opis"));
        }
        if(zadanie.get("dataOddania")!=""){
            objects.setDataOddania(LocalDate.parse(zadanie.get("dataOddania")));
        }

        objects.setDataCzasModyfikacji(LocalDateTime.now());

        return zadanieRepo.save(objects);
    }



}
