package com.example.lab2.service;

import com.example.lab2.Model.Projekt;
import com.example.lab2.Model.Zadanie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public interface ZadanieService {
    Zadanie getZadanie(Integer id) ;
    List<Zadanie> getZadanieById(Integer id);

    Zadanie setZadanie(Zadanie zadanie) ;
    void deleteZadanie(Integer id) ;
    Page<Zadanie> getZadania(Pageable pageable) ;
    Page<Zadanie> searchByNazwa(String nazwa, Pageable pageable);
    List<Zadanie> getAllZadania();
     Zadanie getZadanieByIdI(Integer id) ;
        Zadanie addZadanie(HashMap<String, String> addRequest) throws Exception;
    Zadanie updateZadanie(HashMap<String,String> zadanie,
                          @PathVariable Integer id) throws Exception;

    Page<Zadanie> getZadania1(int page, int size, String sortBy);

    Page<Zadanie> getZadanieById(Integer id, Pageable pageable);

    }