package com.example.lab2.service;

import com.example.lab2.Model.Projekt;
import com.example.lab2.Model.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface ProjektService {

     Optional<Projekt> getProjekt(Integer projektId);

     Projekt getProjektById(Integer projektId);

     Projekt setProjekt(Projekt projekt);

     void deleteProjekt(Integer projektId);

     Page<Projekt> getProjekty(Pageable pageable);

     Page<Projekt> searchByNazwa(String nazwa, Pageable pageable);

     List<Projekt> getAllProjekty();

     Projekt addProjekt(HashMap<String, String> addRequest) throws Exception;

     Projekt updateProjekt(HashMap<String, String> projekt,
                           @PathVariable Integer projektId) throws Exception;

     void joinStudent(Integer id) throws Exception;
     Set<Student> getStudents(Integer id) throws Exception;
     List<Projekt> getStudentProjects() throws Exception;
     Set<Projekt> get1() throws Exception;
     }
