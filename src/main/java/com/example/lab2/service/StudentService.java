package com.example.lab2.service;

import com.example.lab2.Model.Student;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.HashMap;
import java.util.List;

public interface StudentService {
    List<Student> getAllUsers();
    Student findByEmail(String email) throws Exception;
    Student getUserDetailById(int userId) throws Exception;
    Student registerUser(Student student) throws Exception;
    ResponseEntity<? extends Object> sendEmail(Student student) throws Exception;

    }
