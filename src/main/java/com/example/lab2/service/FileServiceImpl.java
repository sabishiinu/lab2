package com.example.lab2.service;

import com.example.lab2.Model.File;
import com.example.lab2.Model.Projekt;
import com.example.lab2.Repository.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.stream.Stream;

import static java.lang.Integer.parseInt;

@Service
public class FileServiceImpl implements FileService {
    @Autowired
    private FileRepository fileDBRepository;
    @Autowired
    private ProjektService projektService;

    @Override
    public File store(MultipartFile newFile, Integer id) throws IOException {
        String fileName = StringUtils.cleanPath(newFile.getOriginalFilename());
        File file = new File();
        Projekt projekt = new Projekt();
        projekt = projektService.getProjektById(id);
        file.setProjekt(projekt);
        file.setName(fileName);
        file.setType(newFile.getContentType());
        file.setData( newFile.getBytes());
        return fileDBRepository.save(file);
    }

    @Override
    public File getFile(String id) {
        return fileDBRepository.findById(id).get();
    }

    @Override
    public Stream<File> getAllFiles() {
        return fileDBRepository.findAll().stream();
    }

    @Override
    public void deleteFile(String id) {
        fileDBRepository.deleteById(id);
    }

}

