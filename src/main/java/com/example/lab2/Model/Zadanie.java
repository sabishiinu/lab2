package com.example.lab2.Model;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "zadanie")
@Setter

@Getter

@AllArgsConstructor

public class Zadanie {
    @Id
    @GeneratedValue
    private Integer zadanieId;

    private String nazwa;
    private String kolejnosc;
    private String opis;
    @CreationTimestamp
    @Column(name = "dataczas_utworzenia", nullable = false, updatable = false)
    private LocalDateTime dataCzasUtworzenia;
    @UpdateTimestamp
    @Column(name = "dataczas_modyfikacji", nullable = false)
    private LocalDateTime dataCzasModyfikacji;
    private LocalDate dataOddania;

    public Zadanie() {
    }
    public Zadanie(String nazwa, String opis, LocalDate dataOddania) {
        this.nazwa = nazwa;
        this.opis = opis;

        this.dataOddania = dataOddania;
    }

    @ManyToOne
    @JoinColumn(name = "projekt_id")
    private Projekt projekt;
}

