package com.example.lab2.Model.dto;

import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
@Setter
@ToString
@NoArgsConstructor
public class StudentDto {

    private Integer studentId;
    private String imie;
    private String nazwisko;
    private String nrIndeksu;
    private String password;
    private String email;
    private Boolean stacjonarny;

    private String role;


    public StudentDto(String imie, String nazwisko, String nrIndeksu, Boolean stacjonarny) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.nrIndeksu = nrIndeksu;
        this.stacjonarny = stacjonarny;
    }

    public StudentDto(String imie, String nazwisko, String nrIndeksu, String email,
                      Boolean stacjonarny) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.nrIndeksu = nrIndeksu;
        this.email = email;
        this.stacjonarny = stacjonarny;
    }


}

