package com.example.lab2.Model.dto;



import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
@Setter
@ToString
@NoArgsConstructor

public class ProjektDto {

    private Integer projektId;
    private String nazwa;
    private String opis;
     private LocalDateTime dataCzasUtworzenia;
   private LocalDateTime dataCzasModyfikacji;
    private LocalDate dataOddania;


    public ProjektDto(String nazwa, String opis, LocalDate dataOddania) {
        this.nazwa = nazwa;
        this.opis = opis;

        this.dataOddania = dataOddania;
    }

}

