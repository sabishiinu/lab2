package com.example.lab2.Model.dto;


import com.example.lab2.Model.Zadanie;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
@Setter
@ToString
@NoArgsConstructor

public class ZadanieDto {

    private Integer zadanieId;

    private String nazwa;
    private String kolejnosc;
    private String opis;

    private LocalDateTime dataCzasUtworzenia;

    private LocalDateTime dataCzasModyfikacji;
    private LocalDate dataOddania;
    private Long projektId;

    public ZadanieDto(ZadanieDto zadanie) {
        this.zadanieId = zadanie.getZadanieId();
        this.nazwa = zadanie.getNazwa();
        this.kolejnosc = zadanie.getKolejnosc();
        this.opis = zadanie.getOpis();
        this.dataCzasUtworzenia = getDataCzasUtworzenia();
        this.dataCzasModyfikacji = getDataCzasModyfikacji();
        this.dataOddania = zadanie.getDataOddania();
        this.projektId = zadanie.getProjektId();



    }

    public ZadanieDto(Zadanie zadanie) {
    }
}

