package com.example.lab2.Model;


import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "projekt")
@Setter

@Getter

@AllArgsConstructor

public class Projekt {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "projekt_id")
    private Integer projektId;
    @Column(nullable = false, length = 50)
    private String nazwa;
    private String opis;
    @CreationTimestamp
    @Column(name = "dataczas_utworzenia", nullable = false, updatable = false)
    private LocalDateTime dataCzasUtworzenia;
    @UpdateTimestamp
    @Column(name = "dataczas_modyfikacji", nullable = false)
    private LocalDateTime dataCzasModyfikacji;
    private LocalDate dataOddania;

    public Projekt() {
    }

    public Projekt(String nazwa, String opis, LocalDate dataOddania) {
        this.nazwa = nazwa;
        this.opis = opis;

        this.dataOddania = dataOddania;
    }

    @OneToMany(fetch=FetchType.LAZY)
    private Set<Zadanie> zadania;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinTable(name = "projekt_student",
            joinColumns = {@JoinColumn(name = "projekt_id")},
            inverseJoinColumns = {@JoinColumn(name = "student_id")})
    private Set<Student> studenci= new HashSet<>();

    public Set<Student> getStudent() {
        return studenci;
    }
    public void addStudent(Student student) {
            this.studenci.add(student);
            student.getProjekty().add(this);

    }
    @OneToMany(fetch=FetchType.LAZY)
    private Set<File> items;

}

