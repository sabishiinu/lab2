package com.example.lab2.Repository;

import com.example.lab2.Model.Projekt;
import com.example.lab2.Model.Student;
import com.example.lab2.Model.Zadanie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ProjektRepository extends JpaRepository<Projekt, Integer> {
    Page<Projekt> findByNazwaContainingIgnoreCase(String nazwa, Pageable pageable);
Projekt findProjektByProjektId(Integer id);

    @Query("SELECT ps FROM Projekt ps ")
    List<Projekt> findProjektsByStudentId();
//@Param("studentId") Integer studentId
     List<Projekt> findByNazwaContainingIgnoreCase(String nazwa);
    @Query("SELECT ps FROM Projekt ps WHERE ps.studenci =:student")

    Projekt findByStudenci(@Param("student") Set<Student> studenci);






}
