package com.example.lab2.controller;

import com.example.lab2.Model.Projekt;
import com.example.lab2.Model.Zadanie;
import com.example.lab2.Model.dto.ZadanieDto;
import com.example.lab2.service.ZadanieService;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/zadanie")
public class ZadanieController {


    @Autowired
    private ZadanieService zadanieService;

    private static final Logger logger = LoggerFactory.getLogger(ZadanieController.class);

    @RequestMapping("/updatezadanie/{zadanieId}")
    public ResponseEntity<?> updateZadanie(@RequestBody HashMap<String, String> zadanie,
                                           @PathVariable Integer zadanieId) {
        try {
            Zadanie obj = zadanieService.updateZadanie(zadanie, zadanieId);

            return ResponseEntity.ok("obj");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new ApiResponse(e.getMessage(), ""));
        }
    }

    @RequestMapping("/getzadanie/{id}")
    public ResponseEntity<?> getZadanie(@PathVariable Integer id,
                                        @RequestBody HashMap<String,Integer> request
    ) {

        try {
            Page<Zadanie> zadanie = zadanieService.getZadanieById(id,PageRequest.of(request.get("page"),request.get("size")));

            return ResponseEntity.ok(new PageImpl<>(zadanie.getContent(), PageRequest.of(request.get("page"),request.get("size")), zadanie.getTotalElements()));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new ApiResponse(e.getMessage(), ""));
        }

    }


/*    @RequestMapping("")
    public List<Zadanie> getAll() {
        return zadanieService.getAllZadania();
    }*/


    @RequestMapping("")
    public ResponseEntity<?> getAll(
            @RequestBody HashMap<String,Integer> request
)  {



        Page<Zadanie> zadaniePage = this.zadanieService.getZadania1(request.get("page"),request.get("size"),"zadanieId");
        return ResponseEntity.ok(new PageImpl<>(zadaniePage.getContent(), PageRequest.of(request.get("page"),request.get("size")), zadaniePage.getTotalElements()));
    }

    @RequestMapping("/add")
    public ResponseEntity<?> addNewProject(@RequestBody HashMap<String, String> addRequest) {
        try {
            Zadanie zadanie = zadanieService.addZadanie(addRequest);
            return ResponseEntity.ok(zadanie);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new ApiResponse(e.getMessage(), ""));
        }
    }


    @RequestMapping("/deletezadanie/{id}")
    ResponseEntity<?> delete(@PathVariable Integer id) {

        zadanieService.deleteZadanie(id);

        return ResponseEntity.noContent().build();
    }
}

