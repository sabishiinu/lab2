package com.example.lab2.controller;
import com.example.lab2.JWTConfiguration.CustomUserDetailsService;
import com.example.lab2.JWTConfiguration.UserPrincipal;
import com.example.lab2.Model.Student;
import org.json.JSONObject;

import com.example.lab2.JWTConfiguration.AuthManager;
import com.example.lab2.JWTConfiguration.JwtTokenProvider;
import com.example.lab2.service.StudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class UserController {
    @Autowired
    private StudentService studentService;
    @Autowired
    AuthManager aMan;
    @Autowired
    JwtTokenProvider tokenProvider;
    @Autowired
    CustomUserDetailsService userDetailservice;
    @Autowired
    StudentService studentservice;
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    @GetMapping("/users1")
    public List<Student> getAllUsers(){
        return studentService.getAllUsers();
    }

    // create user rest api
    @PostMapping("/users")
    public Student createUser(@RequestBody Student student) throws Exception {
        logger.info("register " + student);
        return studentService.registerUser(student);
    }

    // get user by id rest api
    @GetMapping("/users/{id}")
    public ResponseEntity<Student> getUserById(@PathVariable int id) throws Exception {
        Student user = studentService.getUserDetailById(id);
        return ResponseEntity.ok(user);
    }
    @RequestMapping("login/user")//post and get
    public Object userLogin(@RequestBody LoginRequest loginRequest) {

        try {
            logger.info("user is going to validate(usercontr) "+ loginRequest.getEmail());
            Authentication authentication =  aMan.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String token = tokenProvider.generateToken(authentication);

            JSONObject obj =  this.getUserResponse(token);
            if(obj == null) {
                throw new Exception("Error while generating Reponse");
            }

            return new ResponseEntity<String>(obj.toString(), HttpStatus.OK);
        }catch(Exception e ) {
            logger.info("Error in authenticateUser ",e);
            return ResponseEntity.badRequest().body(new ApiResponse(e.getMessage(), ""));
        }
    }
        @RequestMapping("userDetails")
        private JSONObject getUserResponse(String token) {

            try {
                Student user = studentService.getUserDetailById(_getUserId());
                HashMap<String,String> response = new HashMap<String,String>();
                response.put("user_id", ""+_getUserId());
                response.put("email", user.getEmail());
                response.put("name", user.getImie());
                response.put("lastname",user.getNazwisko());
                response.put("role",user.getRole());


                JSONObject obj = new JSONObject();

                obj.put("user_profile_details",response);
                obj.put("token", token);
                return obj;
            } catch (Exception e) {
                logger.info("Error in getUserResponse ",e);
            }
            return null;
        }

        private int _getUserId() {
            logger.info("user id vaildating. "+ SecurityContextHolder.getContext().getAuthentication());
            UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            logger.info("(LoginController)user id is "+userPrincipal.getId());
            return userPrincipal.getId();
        }



    }


    // PRZED KAŻDĄ Z PONIŻSZYCH METOD JEST UMIESZCZONA ADNOTACJA (@GetMapping, PostMapping, ... ), KTÓRA OKREŚLA
// RODZAJ METODY HTTP, A TAKŻE ADRES I PARAMETRY ŻĄDANIA
//Przykład żądania wywołującego metodę: GET http://localhost:8080/api/projekty/1
/*    @GetMapping("/projekty/{projektId}")
    ResponseEntity<Projekt> getProjekt(@PathVariable Integer projektId) {// @PathVariable oznacza, że wartość
        return ResponseEntity.of(projektService.getProjekt(projektId)); // parametru przekazywana jest w ścieżce
    }

    // @Valid włącza automatyczną walidację na podstawie adnotacji zawartych
// w modelu np. NotNull, Size, NotEmpty itd. (z javax.validation.constraints.*)
    @PostMapping(path = "/projekty")
    ResponseEntity<Void> createProjekt(@Valid @RequestBody Projekt projekt) {// @RequestBody oznacza, że dane // projektu (w formacie JSON) są
        Projekt createdProjekt = projektService.setProjekt(projekt); // przekazywane w ciele żądania
        URI location = ServletUriComponentsBuilder.fromCurrentRequest() // link wskazujący utworzony projekt
                .path("/{projektId}").buildAndExpand(createdProjekt.getProjektId()).toUri();
        return ResponseEntity.created(location).build(); // zwracany jest kod odpowiedzi 201 - Created
    } // z linkiem location w nagłówku

    @PutMapping("/projekty/{projektId}")
    public ResponseEntity<Void> updateProjekt(@Valid @RequestBody Projekt projekt,
                                              @PathVariable Integer projektId) {
        return projektService.getProjekt(projektId)
                .map(p -> {
                    projektService.setProjekt(projekt);
                    return new ResponseEntity<Void>(HttpStatus.OK); // 200 (można też zwracać 204 - No content)
                })
                .orElseGet(() -> ResponseEntity.notFound().build()); // 404 - Not found
    }

    @DeleteMapping("/projekty/{projektId}")
    public ResponseEntity<Void> deleteProjekt(@PathVariable Integer projektId) {
        return projektService.getProjekt(projektId).map(p -> {
            projektService.deleteProjekt(projektId);
            return new ResponseEntity<Void>(HttpStatus.OK); // 200
        }).orElseGet(() -> ResponseEntity.notFound().build()); // 404 - Not found
    }

    //Przykład żądania wywołującego metodę: http://localhost:8080/api/projekty?page=0&size=10&sort=nazwa,desc
    @GetMapping(value = "/projekty")
    Page<Projekt> getProjekty(Pageable pageable) { // @RequestHeader HttpHeaders headers – jeżeli potrzebny
        return projektService.getProjekty(pageable); // byłby nagłówek, wystarczy dodać drugą zmienną z adnotacją
    }

    // Przykład żądania wywołującego metodę: GET http://localhost:8080/api/projekty?nazwa=webowa
// Metoda zostanie wywołana tylko, gdy w żądaniu będzie przesyłana wartość parametru nazwa.
    @GetMapping(value = "/projekty", params = "nazwa")
    Page<Projekt> getProjektyByNazwa(@RequestParam String nazwa, Pageable pageable) {
        return projektService.searchByNazwa(nazwa, pageable);
    }*/

