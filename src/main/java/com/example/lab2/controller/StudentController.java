package com.example.lab2.controller;

import com.example.lab2.Model.Projekt;
import com.example.lab2.service.ProjektService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/projekty")
public class StudentController {
    @Autowired
    private ProjektService projektService;


    private static final Logger logger = LoggerFactory.getLogger(ProjektController.class);

    @RequestMapping("/jointoprojekt")
    public ResponseEntity<?> joinToProject(@RequestBody HashMap<String,String> addRequest) {
        try {

            return  ResponseEntity.ok("projekt");
        }catch(Exception e ) {
            return ResponseEntity.badRequest().body(new ApiResponse(e.getMessage(), ""));
        }
    }

}
