package com.example.lab2.controller;

import com.example.lab2.Model.Projekt;
import com.example.lab2.Model.Student;
import com.example.lab2.Repository.ProjektRepository;
import com.example.lab2.service.ProjektService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/projekty")
public class ProjektController {

    @Autowired
    private ProjektService projektService;
    @Autowired
    private ProjektRepository projektRepo;


    @Autowired
    public ProjektController(ProjektService projektService) {
        this.projektService = projektService;
    }

    private static final Logger logger = LoggerFactory.getLogger(ProjektController.class);

    @RequestMapping("/updateprojekty/{projektId}")
    public ResponseEntity<?> updateProjekt(@RequestBody HashMap<String,String> projekt,
                                           @PathVariable Integer projektId) {
        try {
            Projekt obj = projektService.updateProjekt(projekt,projektId);
            return ResponseEntity.ok(obj);
        }catch(Exception e ) {
            return ResponseEntity.badRequest().body(new ApiResponse(e.getMessage(), ""));
        }
    }
    @RequestMapping("/getprojekty/{projektId}")
    public ResponseEntity<?> getProjekt(@PathVariable Integer projektId) {
        return projektService.getProjekt(projektId)
                .map(p -> {
                    logger.info("OK "+p);
                    return ResponseEntity.ok(p);
                })
                .orElseGet(() -> ResponseEntity.notFound().build()); // 404 - Not found
    }

/*
    @DeleteMapping("/projekty/{projektId}")
    public ResponseEntity<Void> deleteProjekt(@PathVariable Integer projektId) {
        return projektService.getProjekt(projektId).map(p -> {
            projektService.deleteProjekt(projektId);
            return new ResponseEntity<Void>(HttpStatus.OK); // 200
        }).orElseGet(() -> ResponseEntity.notFound().build()); // 404 - Not found
    }
*/



    @RequestMapping("/getAll")
    public ResponseEntity<?> getAll() {

            return  ResponseEntity.ok(projektService.getAllProjekty());

    }
    @RequestMapping("/listanazw")
    public ResponseEntity<?> getAllName() throws Exception {

        try {
            return  ResponseEntity.ok(projektService.getAllProjekty());
        }catch(Exception e ) {
            return ResponseEntity.badRequest().body(new ApiResponse(e.getMessage(), "Brak dostępu"));
        }
    }

    @RequestMapping("/addprojekt")
    public ResponseEntity<?> addNewProject(@RequestBody HashMap<String,String> addRequest) {
        try {
            Projekt projekt = projektService.addProjekt(addRequest);
            return  ResponseEntity.ok(projekt);
        }catch(Exception e ) {
            return ResponseEntity.badRequest().body(new ApiResponse(e.getMessage(), ""));
        }
    }


    @RequestMapping("/projekt/{id}")
    ResponseEntity<?> delete(@PathVariable Integer id) {

        projektService.deleteProjekt(id);

        return ResponseEntity.noContent().build();
    }
    @RequestMapping("/projekt/join/{id}")
    ResponseEntity<?> joinStudent(@PathVariable Integer id) throws Exception {

        projektService.joinStudent(id);

        return ResponseEntity.ok("ok");
    }

    @RequestMapping("/projekt/getstudenci/{id}")
    ResponseEntity<?> getStudentsProject(@PathVariable Integer id) throws Exception {

        Set<Student> students = projektService.getStudents(id);

        return ResponseEntity.ok(students);
    }

    @RequestMapping("/projekts/getstudent/")
    ResponseEntity<?> getStudentProjects() {

        Set<Projekt> students ;
        try {
            students = projektService.get1();
            logger.info("OK "+students);

            return ResponseEntity.ok(students);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new ApiResponse(e.getMessage(), ""));
        }
    }
}
