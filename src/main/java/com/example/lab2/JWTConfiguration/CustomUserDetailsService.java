package com.example.lab2.JWTConfiguration;

import com.example.lab2.Model.Student;
import com.example.lab2.service.StudentService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    StudentService userService;
    private static final Logger logger = LoggerFactory.getLogger(CustomUserDetailsService.class);
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String email)
            throws UsernameNotFoundException {
        try {
            Student user = userService.findByEmail(email);
            return UserPrincipal.create(user);
        }catch(Exception e) {
            throw new UsernameNotFoundException("User not found with email : " + email);
        }
    }

    @Transactional
    public UserDetails loadUserById(int id) {
        try {
            Student user = userService.getUserDetailById(id);
            return UserPrincipal.create(user);
        }catch(Exception e) {
            throw new UsernameNotFoundException("User not found with id : " + id);
        }
        //return UserPrincipal.create(user);
    }

//	@Override
//	public UserDetails loadUserByUsername(String mobile) throws UsernameNotFoundException {
//		// TODO Auto-generated method stub
//		return null;
//	}

}
