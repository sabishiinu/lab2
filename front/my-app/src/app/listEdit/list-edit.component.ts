import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationStart, Router} from "@angular/router";
import {AuthService} from "../services/auth.service";
import {filter} from "rxjs";

@Component({
  selector: 'app-list-edit',
  templateUrl: './list-edit.component.html',
  styleUrls: ['./list-edit.component.scss']
})
export class ListEditComponent implements OnInit {
  name = '';
  desc = '';

  oddaj = '';
  id: any;
  projekt: any;
status:any;
  constructor(private router: Router, private http: AuthService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('projectId');

    if (this.id != null) {
      this.getProject(this.id);
      console.log("tak "+this.projekt)
      this.status="update"
    }
    else {
      this.status = "add"
    }
  }


  addProjekt() {


    const request = {
      nazwa: this.name,
      opis: this.desc,
      dataOddania: this.oddaj,
    };
if (this.status == "add"){
  if (this.name == '') {
    alert('Uzupełnij nazwę');
    return;
  }
  if (this.desc == '') {
    alert('Uzupełnij opis');
    return;
  }
  this.http.postRequestWithToken('api/projekty/addprojekt', request).subscribe(data => {
    alert('Zapisano pomyślnie.');
  }, error => {
    alert('Error in save projects ' + error);
  });
}
else {
  console.log("req "+JSON.stringify(request))
  this.http.postRequestWithToken('api/projekty/updateprojekty/'+this.id, request).subscribe(data => {
    alert('Zapisano pomyślnie.');
  }, error => {
    alert('Error in save projects ' + error);
  });
}

  }

  getProject(id:number) {
    this.http.postRequestWithToken('api/projekty/getprojekty/'+id, {}).subscribe(data => {
      this.projekt = data
console.log(this.projekt.nazwa)

    }, error => {
      alert('Error in save projects ' + error);
    });
  }
}
