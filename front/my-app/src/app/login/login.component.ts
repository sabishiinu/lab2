import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  isLogin = false;
  isAdmin = false;
  email = '';
  password = '';
  welcomeUsername = '';
  constructor(private router: Router, private http: AuthService) {
    const request = {};
    this.checkIsLogin();
  }

  checkIsLogin(){
    if (this.http.isLogin()) {
      if (this.http.isAdminUser()) {this.isAdmin = true; }
      this.isLogin = true;
      this.welcomeUsername = this.http.getLoginDataByKey('name');
    }
  }
  ngOnInit(): void {
  }
  loginUserCheck(): any {
  //  window.location.reload();

  if (this.email == '') {
  alert('Email should not be empty');
  return;
}
if (this.password == '') {
  alert('Password should not be empty');
  return;
}

const request = {
  email: this.email,
  password: this.password
};
this.http.postRequest('api/login/user', request).subscribe(data => {
  if (data.hasOwnProperty('token')) {
    // @ts-ignore
    this.http.setLoginToken(data.token);
    this.http.setLoginData(data);
    console.log('TOKEN ' + this.http.getLoginToken());
    this.welcomeUsername = this.http.getLoginDataByKey('name');

    this.isLogin = true;
    if (this.http.isAdminUser())
    {
      this.isAdmin = true;
      this.router.navigate(['admin']);
    }else {
      this.router.navigate(['list']);

    }

  }
}, error => {
  alert('Error in login ' + error);
}); }

}
