import { Component, OnInit } from '@angular/core';
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../services/auth.service";

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class StudentComponent implements OnInit {
  projectId:any;
  studentsList:any;
  userLogin:boolean=false;
  id: any;
  private routeSub: Subscription;

  constructor(private router: Router, private authService: AuthService,private route: ActivatedRoute) {
    this.routeSub = this.route.params.subscribe(params => {
      console.log(params) //log the entire params object
      console.log("PARA "+params['projectId']) //log the value of id
      this.id=params['projectId'];
      this.getAllObject(params['projectId']);

    });
  }
  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      console.log(params) //log the entire params object
      console.log("PARA "+params['projectId']) //log the value of id
      this.id=params['projectId'];
      console.log("id "+this.id)
    });
  }
  getAllObject(id:any) {
    this.authService.postRequestWithToken('api/projekty/projekt/getstudenci/'+id, {}).subscribe(data => {
      console.log(data)
      this.studentsList = data;
    });
    this.userLogin=this.authService.isLogin();
  }
  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }

  addNewTask(){
    location.href=this.id+'/task/add';
  }
  editTask(id: string){
    location.href=+id+'/task/edit';
  }
  joinToProjekt(){
    this.authService.postRequestWithToken('api/projekty/projekt/join/' + this.id, {}).subscribe(data => {
      alert('Dołączono do projektu.');
    }, error => {
      alert('Error in join student ' + error);
    });
    window.location.reload();
    return true;
  }
}
