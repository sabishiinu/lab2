import { Component, OnInit } from '@angular/core';
import { UploadFileService } from 'src/app/services/upload-file.service';
import {HttpEventType, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable, Subscription} from 'rxjs';
import {AuthService} from "../services/auth.service";
import {IconDefinition, faDownload, faTrash} from "@fortawesome/free-solid-svg-icons";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-upload-files',
  templateUrl: './upload-files.component.html',
  styleUrls: ['./upload-files.component.scss']
})
export class UploadFilesComponent implements OnInit {

  selectedFiles: FileList | any;
  currentFile: File | any;
  progress = 0;
  message = '';
  faTrash: IconDefinition = faTrash;
  faDownload: IconDefinition = faDownload;
  fileInfos: Observable<any> | undefined;
  private routeSub: Subscription;
  id: any;
  constructor(private authService: AuthService, private uploadService: UploadFileService,private route: ActivatedRoute) {
    this.routeSub = this.route.params.subscribe(params => {
    console.log(params) //log the entire params object
    console.log("PARA " + params['projectId']) //log the value of id
    this.id = params['projectId'];
  });}

  ngOnInit(): void {
    this.fileInfos = this.uploadService.getFiles();



  }
  selectFile(event: any) {
    this.selectedFiles = event.target.files;
  }
  upload() {
    this.progress = 0;

    this.currentFile = this.selectedFiles.item(0);
    this.uploadService.upload(this.currentFile,this.id).subscribe(
      event => {
        if (event.type === HttpEventType.UploadProgress) {
          // @ts-ignore
          this.progress = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
          this.message = event.body.message;
          this.fileInfos = this.uploadService.getFiles();
        }
      },
      err => {
        this.progress = 0;
        this.message = 'Could not upload the file!';
        this.currentFile = undefined;
      });

    this.selectedFiles = undefined;
  }
  download(id:any){
    this.authService.postRequestWithToken('api/file/files/'+id,{})
      .subscribe(message => {
        console.log(id)
        alert('Dodano pomyślnie.');
      }, error => {
        console.log(error)

        alert('Error in delete file ' + error.message);
      });
  }


  delete(id: string){
    this.authService.postRequestWithToken('api/file/delete/' + id, {}).subscribe(data => {
      alert('Usunięto pomyślnie.');
    }, error => {
      alert('Error in delete file ' + error);
    });
    window.location.reload();
    return true;
  }
  }
