import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../services/auth.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.scss']
})
export class TaskEditComponent implements OnInit {
  name = '';
  desc = '';

  oddaj = '';
  id: any;

  projekt: any;
  status:any;
  private routeSub: Subscription;

  constructor(private router: Router, private http: AuthService,private route: ActivatedRoute) {
    this.routeSub = this.route.params.subscribe(params => {
      console.log(params) //log the entire params object
      console.log("PARA "+params['projectId']) //log the value of id

    });
  }

  ngOnInit(): void {

    this.projekt = this.route.snapshot.paramMap.get("projectId")
    this.id = this.route.snapshot.routeConfig?.path

console.log("edit "+ JSON.stringify(this.id))
    if ( JSON.stringify(this.id) == '":projectId/task/add"') {
      this.status="add"
    }
    else {
      this.status = "update"
    }
  }


  addZadanie() {


    const request = {
      nazwa: this.name,
      opis: this.desc,
      kolejnosc: 1,
      dataOddania: this.oddaj,
      projektId: this.projekt
    };
    console.log("req "+JSON.stringify(request))

    if (this.status == "add"){
      if (this.name == '') {
        alert('Uzupełnij nazwę');
        return;
      }
      if (this.desc == '') {
        alert('Uzupełnij opis');
        return;
      }
      this.http.postRequestWithToken('api/zadanie/add', request).subscribe(data => {
        alert('Zapisano pomyślnie.');
      }, error => {
        alert('Error in save projects ' + error);
      });
    }
    else {
      console.log("req "+request)
      this.http.postRequestWithToken('api/zadanie/updatezadanie/'+this.projekt, request).subscribe(data => {
        alert('Zapisano pomyślnie.');
      }, error => {
        alert('Error in save projects ' + error);
      });
    }

  }

  getProject(id:number) {
    this.http.postRequestWithToken('api/projekty/getprojekty/'+id, {}).subscribe(data => {
      this.projekt = data
      console.log(this.projekt.nazwa)

    }, error => {
      alert('Error in save projects ' + error);
    });
  }
}
