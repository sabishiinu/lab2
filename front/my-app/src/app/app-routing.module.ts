import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component";
import {ListEditComponent} from "./listEdit/list-edit.component";
import {ListComponent} from "./list/list.component";
import {TaskComponent} from "./task/task.component";
import {TaskEditComponent} from "./task-edit/task-edit.component";
import {StudentComponent} from "./student/student.component";
import {AdminComponent} from "./admin/admin.component";
import {JoinListComponent} from "./join-list/join-list.component";
import {UploadFilesComponent} from "./upload-files/upload-files.component";
import {ChatComponent} from "./chat/chat.component";

const routes: Routes = [
  {path: "login", component: LoginComponent},
  {path: "register", component: RegisterComponent},
  {path: "list", component: ListComponent},
  {path: "add", component: ListEditComponent},
  {path: "list/:projectId/edit", component: ListEditComponent},
  {path: "list/:projectId/task", component: TaskComponent},
  {path: ":projectId/task/add", component: TaskEditComponent},
  {path: ":projectId/task/edit", component: TaskEditComponent},
  {path: "list/:projectId/student", component: StudentComponent},
  {path: "", component: LoginComponent},
  {path: "admin", component: AdminComponent},
  {path: "join", component: JoinListComponent},
  {path: "list/:projectId/file", component: UploadFilesComponent},
  {path: "chat", component: ChatComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
