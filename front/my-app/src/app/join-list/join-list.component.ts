
import { Component, OnInit } from '@angular/core';
import {AuthService} from "../services/auth.service";
import {Router} from "@angular/router";
import {
  IconDefinition,
  faEdit,
  faTrash,
  faListCheck,
  faUser
} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-join-list',
  templateUrl: './join-list.component.html',
  styleUrls: ['./join-list.component.scss']
})
export class JoinListComponent implements OnInit {

  projectList:any;
  userLogin:boolean=false;
  faEdit: IconDefinition = faEdit;
  faTrash: IconDefinition = faTrash;
  faListCheck: IconDefinition = faListCheck;
  faArrowRightToBracket: IconDefinition = faUser;

  constructor(private router: Router, private authService: AuthService) {
    this.getAllObject();

  }
  ngOnInit(): void {
  }
  getAllObject() {
    this.authService.postRequestWithToken('api/projekty/getAll/', {}).subscribe(data => {
      console.log(data)
      this.projectList = data;
    });
    this.userLogin=this.authService.isLogin();
  }
  logout(): any {
    this.authService.logout();
    this.authService.isLogin();
    this.router.navigate(['login']);

  }
  delete(id: any){
    this.authService.postRequestWithToken('api/projekty/projekt/' + id, {}).subscribe(data => {
      alert('Usunięto pomyślnie.');
    }, error => {
      alert('Error in delete project ' + error);
    });
    window.location.reload();
    return true;
  }
  joinToProjekt(id:any){
    this.authService.postRequestWithToken('api/projekty/projekt/join/' + id, {}).subscribe(data => {
      alert('Dołączono do projektu.');
    }, error => {
      alert('Error in join student ' + error);
    });
    window.location.reload();
    return true;
  }
}
