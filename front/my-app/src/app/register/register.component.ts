import { Component, OnInit } from '@angular/core';
import {AuthService} from "../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  name = '';
  lastname = '';
  index = '';
  form = '';
  email= '';
  password= '';
  re_password= '';

  constructor(private router: Router, private http: AuthService) { }

  ngOnInit(): void {
  }
  registerUser() {
    if (this.name == '') {
      alert('Uzupełnij imię');
      return;
    }
    if (this.lastname == '') {
      alert('Uzupełnij nazwisko');
      return;
    }
    if (this.email == '') {
      alert('Email nie powinien być pusty');
      return;
    }
    if (this.password == '') {
      alert('Hasło  nie powinno być puste');
      return;
    }
    if (this.password != this.re_password) {
      alert('Hasło i jego potwierdzenie musi być takie samo');
      return;
    }
    if (this.index == '') {
      alert('Numer indeksu nie powinien być pusty');
      return;
    }
    if (this.form == '') {
      alert('Wybierz formę studiow');
      return;
    }

    const request = {
      imie: this.name,
      nazwisko: this.lastname,
      email: this.email,
      password: this.password,
      nrIndeksu: this.index,
      stacjonarny: this.form,
    };
    this.http.postRequest('api/users', request).subscribe(data => {
      alert('Zarejestrowano pomyślnie.');
    }, error => {
      alert('Error in register user ' + error);
    });
  }

}
