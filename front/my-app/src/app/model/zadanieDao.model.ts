
export class ZadanieDao {
  id: number | undefined;
  name: string | undefined;
  desc: string | undefined;
  created: string | undefined;
  modified: string | undefined;
}
