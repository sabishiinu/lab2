import { Component, OnInit } from '@angular/core';
import {IconDefinition, faEdit, faTrash} from "@fortawesome/free-solid-svg-icons";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../services/auth.service";
import {Subscription} from "rxjs";
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { PagedResponse } from '../model/paged-response-model';
import {HttpClient} from "@angular/common/http";
import {ZadanieDao} from "../model/zadanieDao.model";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  totalElements: number = 5;
  todos: ZadanieDao[] = [];
  loading: boolean | undefined;
  page: number = 0;
  size: number = 4;
  totalCount: number = 0;
  zad: any;
  projectId: any;
  projectList: any;
  userLogin: boolean = false;
  faEdit: IconDefinition = faEdit;
  faTrash: IconDefinition = faTrash;
  id: any;
  private routeSub: Subscription;

  constructor(private router: Router, private authService: AuthService, private route: ActivatedRoute, private http: HttpClient,) {
    this.routeSub = this.route.params.subscribe(params => {
      console.log(params) //log the entire params object
      console.log("PARA " + params['projectId']) //log the value of id
      this.id = params['projectId'];
    });
  }

  ngOnInit(): void {
    const request = {
      page: 0,
      size: 5
    };
    this.routeSub = this.route.params.subscribe(params => {
      console.log(params) //log the entire params object
      console.log("PARA " + params['projectId']) //log the value of id
      this.id = params['projectId'];
      console.log("id " + this.id)
    });
    this.getAllObject(this.id, request);
  }

  getAllObject(id:any,request:any) {
    this.authService.postRequestWithToken('api/zadanie/getzadanie/'+id, request).subscribe(data => {
      console.log(data)
      // @ts-ignore
      this.projectList = data.content;


      // @ts-ignore
     // this.zad = data.content;
      // @ts-ignore
      this.totalElements = data.totalElements;
      this.loading = false;
    });
    this.userLogin=this.authService.isLogin();

  }
    ngOnDestroy() {
      this.routeSub.unsubscribe();
    }

  addNewTask(){
    location.href=this.id+'/task/add';
  }
  editTask(id: string){
    location.href=+id+'/task/edit';
  }
  deleteTask(id: string){
    this.authService.postRequestWithToken('api/zadanie/deletezadanie/' + id, {}).subscribe(data => {
      alert('Usunięto pomyślnie.');
    }, error => {
      alert('Error in delete task ' + error);
    });
    window.location.reload();
    return true;
  }






  // tslint:disable-next-line:typedef
  nextPage(event: PageEvent) {
    const request = {
      page: event.pageIndex,
      size: event.pageSize
    };

    this.totalElements = event.pageSize;
    this.getAllObject(this.id,request);
  }
  public getDevelopers(request:any): void {
    this.loading = true;

    this.authService.postRequestWithToken('api/zadanie',request)
      .subscribe(data => {
        // @ts-ignore
        this.zad = data.content;
        // @ts-ignore
        this.totalElements = data.totalElements;
        this.loading = false;
      })
  }
}
