import {getTestBed, TestBed} from '@angular/core/testing';

import { AuthService } from './auth.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HeaderComponent} from '../header/header.component';

describe('AuthService', () => {
  let service: AuthService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [HeaderComponent],
      providers: [AuthService]
    });
    httpMock = getTestBed().get(HttpClientTestingModule);
    service = TestBed.inject(AuthService);
  });

     it('should be created', () => {
       expect(service).toBeTruthy();
     });

  });

