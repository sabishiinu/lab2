import { Injectable } from '@angular/core';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor() { }
  signOut(): void {
    window.sessionStorage.clear();
  }

  public saveToken(token: string): void {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }

  public getToken(): string | null {
    return window.sessionStorage.getItem(TOKEN_KEY);
  }

  public saveUser(user: any): void {
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
  }

  public getUser(): any {
    const user = window.sessionStorage.getItem(USER_KEY);
    if (user) {
      return JSON.parse(user);
    }

    return {};
  }
  setLoginData(data:any) {
    localStorage.setItem('login_data', JSON.stringify(data.user_profile_details));
  }

  getLoginDataByKey(key:any) {
    // @ts-ignore
    const data = JSON.parse(localStorage.getItem('login_data'));

    if (data.hasOwnProperty(key)) {
      return data[key];
    }
    return null;
  }
  setLoginToken(token:any) {
    if (token != '') {
      localStorage.setItem('token', token);
    }
  }
  getLoginToken() {
    return localStorage.getItem('token');
  }
  logout() {
    localStorage.setItem('token', '');
    this.setLoginToken('');
    this.setLoginData('');
  }
  isLogin() {
    try {
      console.log('login token ' + this.getLoginToken());
      // @ts-ignore
      if (this.getLoginToken() != '' && this.getLoginToken().length > 10) {
        return true;
      }
    } catch (e) {

    }
    return false;
  }
}
