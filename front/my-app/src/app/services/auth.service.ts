import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {throwError} from 'rxjs';
import {TokenStorageService} from './token-storage.service';
import { catchError } from 'rxjs/operators';
const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

// tslint:disable-next-line:variable-name
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  url = 'http://localhost:8080/';

  constructor(private http: HttpClient, private token: TokenStorageService) { }

       postRequest(url: string, param: {}) {
    return this.http.post(this.url + url, param, httpOptions);

  }
  postRequestWithToken(url: string, param: any) {
    const httpOptionsWithToken = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Response-Type': 'ResponseContentType.Blob',
        Authorization: 'Bearer ' + this.token.getLoginToken()

      })
    };


    return this.http.post(this.url + url, param, httpOptionsWithToken)
      .pipe(
        catchError(async (err) => this.handleError.bind(err))
      );
  }


  private handleError(error: any) {
    if (error.error instanceof ErrorEvent) {
      return console.error('An error occurred:', error.error.message);

    } else {
      return throwError('Something went wrong..while connecting with server');
    }
  }

  setLoginData(data: any) {
    localStorage.setItem('login_data', JSON.stringify(data.user_profile_details));
  }
  getLoginDataByKey(key: any) {
if(localStorage.getItem('login_data')!=undefined){
  const data = JSON.parse(<string>localStorage.getItem('login_data'));
 if(data != null){
  if (data.hasOwnProperty(key)) {
    return data[key];
  }
  return null;
}}
  return null;
  }


  setLoginToken(token: any) {
    if (token != '') {
      localStorage.setItem('token', token);
    }
  }
  getLoginToken() {
    return localStorage.getItem('token');
  }
  logout() {
    localStorage.setItem('token', '');
    this.setLoginToken('');
    this.setLoginData('');
  }
  isLogin() {
    try {
      console.log('login token ' + this.getLoginToken());
      // @ts-ignore
      if (this.getLoginToken() != '' && this.getLoginToken().length > 10) {
        return true;
      }
    } catch (e) {

    }
    return false;
  }
  isAdminUser(): boolean {
    if (this.getLoginDataByKey('role') == 'ADMIN') {
      return true;
    }
    return false;
  }
}
